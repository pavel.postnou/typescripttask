const orderController = require("../controllers/orderController.js");
import * as express from 'express'
const orderRout = express.Router()
const auth = require ("../auth.js");

orderRout.post("/create", auth.verifyToken, orderController.createOrder);
orderRout.delete("/:id", auth.verifyToken, orderController.deleteOrder);
orderRout.get("/:id", auth.verifyToken, orderController.getOrderById);
orderRout.get("/get", auth.verifyToken, orderController.getAllOrders);

export {orderRout};