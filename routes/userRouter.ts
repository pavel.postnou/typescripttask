const userController = require("../controllers/userController");
const auth = require ("../auth.js");
import * as express from 'express'
const userRouter = express.Router()

userRouter.post("/create",auth.verifyToken, auth.isAdmin, userController.createUser);
userRouter.delete("/delete/:id", auth.verifyToken, auth.isAdmin, userController.deleteUser);
userRouter.put("/update", auth.verifyToken, userController.updateUser);
userRouter.get("/getbyid/:id", auth.verifyToken, userController.getUserByID);
userRouter.get("/getallusers", auth.verifyToken, userController.getAllUsers);

export {userRouter};