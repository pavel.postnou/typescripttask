import * as categoryController from "../controllers/categoryController.js";
import * as express from 'express'
const categoryRout = express.Router()
const auth = require ("../auth");
// import * as auth from "../auth";

categoryRout.post("/create",auth.verifyToken,auth.isAdmin,categoryController.createCategory);
categoryRout.delete("/:id",auth.verifyToken,auth.isAdmin, categoryController.deleteCategory);
categoryRout.put("/:id", auth.verifyToken,auth.isAdmin, categoryController.updateCategory);
categoryRout.get("/:id", auth.verifyToken,categoryController.getСategoryById);
categoryRout.get("/get/all", auth.verifyToken,categoryController.getAllСategories);

export {categoryRout};
