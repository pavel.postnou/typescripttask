import * as express from 'express'
const cartRout = express.Router()
const cartController = require("../controllers/cartController.js");
const auth = require ("../auth.js");

cartRout.post("/create", auth.verifyToken, cartController.createCart);
cartRout.get("/:id", auth.verifyToken, cartController.getCartById);
cartRout.put("/add", auth.verifyToken, cartController.addProductToCart);

export {cartRout};