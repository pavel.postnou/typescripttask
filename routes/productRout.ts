const productController = require("../controllers/productController.js");
const auth = require ("../auth.js");
import * as express from 'express'
const productRout = express.Router()

productRout.post("/create",auth.verifyToken,auth.isAdmin ,productController.createProduct);
productRout.delete("/:id",auth.verifyToken,auth.isAdmin, productController.deleteProduct);
productRout.put("/:id",auth.verifyToken,auth.isAdmin, productController.updateProduct);
productRout.get("/:id", auth.verifyToken,productController.getProductById);
productRout.get("/get/all", auth.verifyToken,productController.getAllProducts);

export {productRout};
