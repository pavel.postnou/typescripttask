const homeController = require("../controllers/homeController.js");
import * as express from 'express'
const homeRout = express.Router()

homeRout.get("/", homeController.index);
 
export {homeRout};