import * as express from 'express'
const authRouter = express.Router()

const authController = require("../controllers/authController");

authRouter.post("/register", authController.regUser);
authRouter.post("/login", authController.loginUser);
authRouter.get("/auth", authController.googleLogin);
authRouter.get("/googleauth/", authController.googleAuth);
 
export {authRouter};