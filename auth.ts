import { Request, Response, NextFunction } from 'express'
import * as jwt from 'jsonwebtoken';
import {accessToken} from './config';
import {User,UserInt} from './models/user';
interface CustomRequest<T> extends Request {
  decoded:any
}


const verifyToken = (req:CustomRequest<UserInt>, res:Response, next:NextFunction) => {
  const authHeader = req.headers.authorization;
  const token = authHeader.split(' ')[1];
  if (!token) {
    return res.status(403).send("A token is required for authentication");
  }
  try {
    const decoded = jwt.verify(token, accessToken);
    req.user = decoded;
    req.decoded = decoded;
  } catch (err) {
    return res.status(401).send("Invalid Token");
  }
  return next();
};

const isAdmin = async (req:CustomRequest<UserInt>, res:Response, next:NextFunction) => {
    const admin = await User.findOne({_id: req.decoded.user_id});
    console.log(admin.role)
  if (admin.role !== "Admin") {
    return res.status(403).send("Forbidden. Ask for Admin")
  }
  return next();
}

export default{ verifyToken, isAdmin};