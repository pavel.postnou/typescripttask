import { Request, Response } from 'express'
import {Cart, CartInt} from '../models/cart.js';
import {User,UserInt} from '../models/user.js';
import {Product,ProductInt} from '../models/product.js';

interface CustomRequest <T> extends Request {
    body: T;
    decoded:string|any;
}

exports.createCart = async function (request: CustomRequest<UserInt>, response: Response) {
    try {
        let user = await User.findOne({ _id: request.decoded.user_id });
        if (user.cart) {
            return response.status(400).send("У вас уже есть одна корзина")
        }
        else {
            const newCart = await new Cart({});
            newCart.user = request.decoded.user_id;
            await newCart.save();
            user.cart = newCart._id;
            await user.save();
            response.send(newCart);
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}

exports.getCartById = async function (request: CustomRequest <CartInt>, response: Response) {
    try {
        let cart = await Cart.findOne({ _id: request.params.id });
        if (cart) {
            response.send(`корзина: ${cart}`)
        }
        else {
            response.send("Такой корзины нет")
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}

exports.addProductToCart = async function (request: CustomRequest <ProductInt>, response: Response) {
    try {
        let cartUser = await User.findOne({ _id: request.decoded.user_id });
        let cart = await Cart.findOne({ _id: cartUser.cart});
        let product = await Product.findOne({ prodname: request.body.name });

        if (!cart) {
            response.send("У вас нет корзины");
        } else if (!product) {
            response.send("Такого товара не существует")
        }
        else if (product && cart) {
            cart.products.push(product._id.toString());
            await cart.save();
            response.send(`Товар ${product.name} добавлен в корзину пользователя ${cartUser.name}`)
        }
        else {
            response.send("Такой корзины нет")
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}
