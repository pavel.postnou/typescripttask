import { Request, Response } from 'express'
import {User,UserInt} from '../models/user';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';
import {accessToken} from '../config';

interface CustomRequest<T> extends Request {
  body: T;
  user_id:T;
}

const createUser = async (req:CustomRequest <UserInt> , res: Response) => {
    try {
        const { name, surname, email, password } = req.body;
    if (!(email && password && surname && name)) {
      res.status(400).send("You can't create new User without required input.");
    }
    const oldUser = await User.findOne({ email });
    if (oldUser) {
      return res.status(409).send("User Already Exist. Please email");
    }
    let role:string = req.body.role;
    let encryptedPassword:string = await bcrypt.hash(password, 10);
    const user = await User.create({
      name,
      surname,
      email: email.toLowerCase(), 
      password: encryptedPassword,
      role: role,
    });
    const token = jwt.sign(
      { user_id: user._id, email },
        accessToken,
      {
        expiresIn: "2h",
      }
    );
    user.token = token;
    res.status(201).json(user);
  } catch (err) {
    console.log(err);
  }
};

const deleteUser = async (req:CustomRequest <UserInt> , res: Response) => {
    try {
        const userIdforDelete = await User.findByIdAndDelete(req.params.id);
        res.send(userIdforDelete);
        }
    catch (err) {
        console.log(err);
        res.status(403).send("oh no");
    }   
};

const updateUser = async (req:CustomRequest <UserInt> , res: Response) => {
    try {
        const user = await User.findOne({_id:req.user_id});
        const users = req.body;
        const userIdForUpdate = await User.findByIdAndUpdate(req.user_id, {
            name: ((users.name != null)?(user.name = users.name):(user.name = user.name)),
            surname: ((users.surname != null)?(user.surname = users.surname):(user.surname = user.surname)),
            email: ((users.email != null)?(user.email = users.email):(user.email = user.email)),
        })
        res.send(userIdForUpdate);
    }
    catch (err) {
        console.log(err);
        res.status(403).send("oh no");
    }    
};

const getUserByID = async (req:CustomRequest <UserInt> , res: Response) => {
    try {
            const getUserId= await User.findById(req.params.id); 
            res.send(getUserId);
    }
    catch (err) {
        console.log(err);
        res.status(403).send("oh no");
    }
};

const getAllUsers = async (req:CustomRequest <UserInt> , res: Response) => {
    try {
            const getUsersAll = await User.find();
            res.send(getUsersAll);
    }
    catch (err) {
        res.status(403).send("oh no");
    }
}

module.exports = { createUser, deleteUser, updateUser, getUserByID, getAllUsers };