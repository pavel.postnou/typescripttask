import { Category, CategoryInt} from '../models/category';
import { Request, Response } from 'express'

interface CustomRequest<T> extends Request {
   body: T
}

const createCategory = async function (request:CustomRequest <CategoryInt>, response:Response) {
    try {
        let category = await Category.findOne({ name: request.body.name });
        if (category) {
            response.status(400).send("Такая категория уже существует")
        }
        else {
            const newCategory = new Category({ name: request.body.name});
            await newCategory.save();
            response.send(`Новая категория создана: ${newCategory.name}`);
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
};

const deleteCategory = async function (request:CustomRequest <CategoryInt>, response: Response) {
    try {
        let category = await Category.findOne({_id: request.params.id});
        if (category) {
            await Category.deleteOne({ _id: request.params.id });
            response.send(`Удалена категория ${category.name}`);
        }
        else {
            response.send("Такой категории нет");
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло нет так")
    }
};

const updateCategory = async function (request:CustomRequest <CategoryInt>, response:Response) {
    try {
        let category = await Category.findOne({ _id: request.params.id });
        if (category) {
            Category.updateOne({ _id: request.params.id }, {name:request.body.name});
            await Category.findOne({ _id: request.params.id });
            response.send(`Категория ${category.name} изменена`);
        }
        else {
            response.send("Такой категории нет");
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
};

const getСategoryById = async function (request:CustomRequest <CategoryInt>, response:Response) {
    try {
        let category = await  Category.findOne({ _id: request.params.id });
        if (category) {
            response.send(`Категория: ${category.name}`)
        }
        else {
            response.send("Такой категории нет")
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
};

const getAllСategories = async function (request:CustomRequest <CategoryInt>, response:Response) {
    try {
        let categories = await Category.find();
        if (categories) {
            response.send(`${categories}`)
        }
        else {
            response.send("Категорий не существует")
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
};

export {createCategory, deleteCategory, updateCategory, getСategoryById, getAllСategories}