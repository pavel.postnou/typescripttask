import {Order,OrderInt} from "../models/order.js";
import  {Cart,CartInt} from "../models/cart.js";
import {User, UserInt} from "../models/user.js";
import { Request, Response } from 'express'
let orderNumber = 1;


interface CustomRequest<T> extends Request {
  body: T;
  decoded:string | any;
}

const createOrder = async function (request:CustomRequest <OrderInt>, response:Response) {
  try {
    let order = await Order.findOne({ number: orderNumber });
    let user = await User.findOne({ _id: request.decoded.user_id });
    let cart = await Cart.findOne({ _id: user.cart._id });
    if (order) {
      response.status(400).send("Такой заказ уже существует");
    } else if (!cart) {
      response.status(400).send("Такой корзины не существует");
    } else {
      const newOrder = new Order({
        number: orderNumber,
        user: request.decoded.user_id,
      });
      for (let i:number = 0; i < cart.products.length; i++) {
        newOrder.products.push(cart.products[i]);
      }
      await newOrder.save();
      user.orders.push(newOrder._id);
      await user.save();
      await User.updateOne(
        { cart: user.cart._id },
        { $unset: { cart: user.cart._id } }
      );
      await Cart.deleteOne({ _id: user.cart._id });
      orderNumber++;
      response.send(
        `Заказ сформирован ${newOrder.number}. Покупатель ${user.name}. Товары ${newOrder.products}`
      );
    }
  } catch (err) {
    response.status(400).send("Что-то пошло не так");
  }
};

const deleteOrder = async function (request:CustomRequest <OrderInt>, response:Response) {

  try {
      let user = await User.findOne({ _id: request.decoded.user_id })
      if (user.role !== "admin") {
          let order = await Order.findById({ _id: request.params.id });
          for (let i:number = 0; i < user.orders.length; i++) {
              if (order._id.toString() == user.orders[i].toString()) {
                  let orderDel = await Order.findOneAndDelete({_id: request.params.id})
                  await User.updateOne({_id: request.decoded.user_id}, { $pull: { orders:  request.params.id } })
                  response.status(400).send(`Заказ ${orderDel.number} удалён`)
              }
              else {
                  response.status(400).send("Такого заказа не существует или он не ваш")
              }
          }
      }
      else {
          let order = await Order.findById({ _id: request.params.id });
          if (!order) {
              response.status(400).send("Такого заказа не существует")
          }
          else {
              await User.updateOne({orders:order._id}, { $pull: { orders:  request.params.id } })
              let orderDel = await Order.findOneAndDelete({ _id: request.params.id })
              response.status(400).send(`Заказ ${orderDel.number} удалён`)
          }
      }
  }
  catch (err) {
      response.status(400).send("Что то пошло не так")
  }
}


const getOrderById = async function (request:CustomRequest <OrderInt>, response:Response) {
  try {
    let order = await Order.findOne({ _id: request.params.id });
    if (order) {
      response.send(`заказ: ${order}`);
    } else {
      response.send("Такого заказа не существует");
    }
  } catch (err) {
    response.status(400).send("Что-то пошло не так");
  }
};

const getAllOrders = async function (request:CustomRequest <OrderInt>, response:Response) {
    try {
        let user = await User.findOne({ _id: request.decoded.user_id })
        if (user.role !== "Admin") {
            response.send(user.orders)
        }
        else {
            let orders = await Order.find();
            if (orders) {
                response.send(`${orders}`)
            }
            else {
                response.send("Заказы отсутствуют")
            }
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
};

export {createOrder, deleteOrder, getOrderById, getAllOrders};
