import {User, UserInt} from '../models/user'
import { Request, Response } from 'express'
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';
import {accessToken} from '../config';
const googleClientId = '858074835463-03l01lh3a6luc8817o7534n1d3kka1j4.apps.googleusercontent.com'
const googleSecret = "PuYPeUN1X0eztgmHpbk8Trtr";
const googleRedirect = 'http://localhost:3000/googleauth';
import {google} from'googleapis';
import urlParse = require('url-parse');
import * as queryString from 'query-string';
import axios from 'axios';

interface CustomRequest<T> extends Request {
  body: T 
}

const oauth2Client = new google.auth.OAuth2(
  googleClientId,
  googleSecret,
  googleRedirect);

const url = oauth2Client.generateAuthUrl({
  access_type: 'offline',
  scope: ['https://www.googleapis.com/auth/userinfo.profile',
          'https://www.googleapis.com/auth/userinfo.email'
  ]
});

const regUser = async (req: CustomRequest<UserInt> ,res: Response) => {
    try {
        const { name, surname, email, password } = req.body;
    if (!(email && password && surname && name)) {
      res.status(400).send("All input is required");
    }
    const oldUser = await User.findOne({ email });
    if (oldUser) {
      return res.status(409).send("User Already Exist. Please email");
    }
    let role = req.body.role;
    let encryptedPassword = await bcrypt.hash(password, 10);
    const user = await User.create({
      name,
      surname,
      email: email.toLowerCase(), 
      password: encryptedPassword,
      role: role,
    });
    const token = jwt.sign(
      { user_id: user._id, email },
        accessToken,
      {
        expiresIn: "2h",
      }
    );
    user.token = token;
    res.status(201).json(user);
  } catch (err) {
    console.log(err);
  }
};


const loginUser = async (req: CustomRequest<UserInt>, res: Response) => {
  try {
    const { email, password } = req.body;
    if (!(email && password)) {
      res.status(400).send("All input is required");
    }
    const user = await User.findOne({ email });
    if (user && (await bcrypt.compare(password, user.password))) {
      const token = jwt.sign(
        { user_id: user._id, email },
          accessToken,
        {
          expiresIn: "2h",
        }
      );
      user.token = token;
      res.status(200).json(user);
    }
    res.status(400).send("Invalid Credentials");
  } catch (err) {
    console.log(err);
  }
};

const googleLogin = async (req:Request,res:Response) => {
  res.send(url)
}

const googleAuth = async (req:CustomRequest<UserInt>,res:Response) => {
  try{
    const googleUrl = new urlParse(req.url);
    const code = queryString.parse(googleUrl.query).code;
    const {tokens} = await oauth2Client.getToken(code);

    const {data} = await axios({
      url: 'https://www.googleapis.com/oauth2/v2/userinfo',
      method: 'get',
      headers: {
        authorization: `Bearer ${tokens.access_token}`
      }
    })

    const user = await User.findOne({email: data.email});
    if(!user){
      const createUser = new User({name: data.given_name, surname: data.family_name, email: data.email});
      await createUser.save();
      const accessTokenForUser = jwt.sign({name: createUser.name, role: createUser.role, _id: createUser._id}, accessToken, {expiresIn: '25h'});
      return res.send(`${createUser} ${accessTokenForUser}`)
    } else {
      res.status(400).send('user error')
    }
  }catch (err){
    res.status(400).send('Some error')
  }
}

export { regUser, loginUser, googleLogin, googleAuth };