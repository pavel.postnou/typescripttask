import { Request, Response } from 'express'
import {Category} from '../models/category.js';
import {Product,ProductInt} from '../models/product.js';

interface CustomRequest<T> extends Request {
    body: T
}

const createProduct = async function (request: CustomRequest <ProductInt>, response: Response) {
    try {
        let product = await Product.findOne({ name: request.body.name });

        let category = await Category.findOne({ catname: request.body.category });

        if (!category) {
            response.send("Такой категории не существует") 
        }

       else if (product) {
            response.status(400).send("Такой товар уже существует")
        }
        
        else
        {
            const newProduct = await new Product({ name: request.body.name});
            newProduct.category.push(category._id)
            await newProduct.save();
            category.products.push(newProduct._id);
            await category.save();
            response.send(`Новый товар создан: ${newProduct.name}`);
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}

const deleteProduct = async function (request: CustomRequest <ProductInt>, response: Response) {
    try {
        let product = await Product.findOne({ _id: request.params.id });
        let category = await Category.findOne({ _id: product.category });
        if (product) {
            await Product.deleteOne({ _id: request.params.id });
            for (let i:number = 0; i < category.products.length; i++) {
                if (category.products[i]._id.toString() == product._id.toString()) {
                    category.products.splice((category.products).indexOf(i), 1);
                    await category.save()
                }
            }
            response.send(`Удалён товар ${product.name}`);
        }
        else {
            response.send("Такого товара нет");
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло нет так")
    }
}

const updateProduct = async function (request: CustomRequest<ProductInt>, response: Response) {
    try {
        let product = await Product.findOne({ _id: request.params.id });
        if (product) {
            Product.updateOne({ _id: request.params.id }, { name: request.body.name });
            response.send(`Товар изменён ${product.name}`);
        }
        else {
            response.send("Такого товара нет");
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}

const getProductById = async function (request: CustomRequest <ProductInt>, response: Response) {
    try {
        let product = await Product.findOne({ _id: request.params.id });
        if (product) {
            response.send(`Товар: ${product.name}`)
        }
        else {
            response.send("Такого товара нет")
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}

const getAllProducts = async function (request: CustomRequest <ProductInt>, response: Response) {
    try {
        let products = await Product.find();
        if (products) {
            response.send(`${products}`)
        }
        else {
            response.send("Товары отсутствуют")
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}

export {createProduct,deleteProduct, updateProduct, getProductById, getAllProducts}
