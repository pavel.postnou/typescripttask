import * as mongoose from 'mongoose'
import {CartInt} from '../models/cart.js';
import {OrderInt} from '../models/order.js';

const Schema = mongoose.Schema;

interface UserInt extends mongoose.Document {
    name: string;
    surname:string;
    email:string;
    password:string;
    token:string;
    role:string;
    cart:CartInt;
    orders:Array<OrderInt>;
  }

  const UserSchema = new Schema({
    name: {
      type: String,
      default: "NoName",
      minlength: 2,
      maxlength: 35
      },
    surname: {
      type: String,
      default: "NoSurname",
      minlength: 2,
      maxlength: 35
      },
    email: { 
      type: String, 
      unique: true
      },
    password: { 
      type: String,
   },
    token: { type: String },
    role: {
      type: String,
      default: "User"
    },
    cart: {
      type: mongoose.Schema.Types.ObjectId, ref: "Cart",
    },
    orders: [{
        type: mongoose.Schema.Types.ObjectId, ref: "order",
        autopopulate: true
    }]
  }, { versionKey: false });
  
  const User = mongoose.model<UserInt>("User", UserSchema);
  UserSchema.plugin(require('mongoose-autopopulate'));
  
  export {User, UserInt};