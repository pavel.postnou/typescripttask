import * as mongoose from 'mongoose'
const Schema = mongoose.Schema;
import {ProductInt} from '../models/product.js';

interface CategoryInt extends mongoose.Document {
    name: string;
    products: Array<ProductInt>|any;
  }

  const categoryScheme = new Schema({
    catname: {
        type: String,
        required: true,
        minlength:1,
        maxlength:20,
        default:"noname"
    },
    products :[{
        type: mongoose.Schema.Types.ObjectId, ref:"Product",
    }]
},
{versionKey: false }
);

const Category = mongoose.model<CategoryInt>("Category", categoryScheme);
export {Category,CategoryInt}
