import * as mongoose from 'mongoose'
const Schema = mongoose.Schema;

interface OrderInt extends mongoose.Document {
    number: number;
    products: Array<string>;
    cartId: string;
    user: string;
  }

  const orderScheme = new Schema({
    number: {
        type:Number
    },
    products: [{
        type: mongoose.Schema.Types.ObjectId, ref: "Cart",
    
    }],
    cartId: {
        type: mongoose.Schema.Types.ObjectId, ref: "Cart"
    },
    user:{
        type: mongoose.Schema.Types.ObjectId, ref: "User"
    }

},
    { versionKey: false }
);

const Order = mongoose.model<OrderInt>("Order", orderScheme);
export {Order, OrderInt}