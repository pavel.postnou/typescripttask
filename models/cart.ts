import * as mongoose from "mongoose";
const Schema = mongoose.Schema;
import {UserInt} from '../models/user.js';
import {ProductInt} from '../models/product.js';

interface CartInt extends mongoose.Document {
    user: UserInt;
    products: Array<string>;
  }

const cartScheme = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId, ref: "User",
        
    },
    products: [{
        type: mongoose.Schema.Types.ObjectId, ref: "Product",
    }]
},
    { versionKey: false }
);

const Cart = mongoose.model<CartInt>("Cart", cartScheme);
export {Cart, CartInt};
