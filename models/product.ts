import * as mongoose from 'mongoose'
const Schema = mongoose.Schema;

interface ProductInt extends mongoose.Document {
    name: string;
    category: Array<string>;
  }

  const productScheme = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 30,
        default: "noname"
    },
    category: [{
        type: mongoose.Schema.Types.ObjectId, ref: "Category",
    }]
},
    { versionKey: false }
);
const Product = mongoose.model<ProductInt>("Product", productScheme);
export {Product, ProductInt};