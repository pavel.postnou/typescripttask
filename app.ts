import * as mongoose from "mongoose";
import * as bodyParser from 'body-parser';
const express = (require('express'));
const app = express();
const urlencodedParser = bodyParser.urlencoded({extended: true});


mongoose.connect("mongodb://localhost:27017/task10db",
{ useUnifiedTopology: true, 
    useNewUrlParser: true, 
    useFindAndModify: true })
    .then(() => {
        console.log("Successfully connected to database");
    })
    .catch((error:string) => {
        console.log("database connection failed. exiting now...");
        console.error(error);
        process.exit(1);
    });
    

    import {authRouter} from './routes/authRouter';
    import {userRouter} from './routes/userRouter';
    import {cartRout} from './routes/cartRout';
    import {categoryRout} from './routes/categoryRout';
    import {homeRout} from './routes/homeRout';
    import {orderRout} from './routes/orderRout';
    import {productRout} from './routes/productRout';
    app.use('', urlencodedParser, authRouter);
    app.use('/user', urlencodedParser, userRouter);
    app.use('/cart', urlencodedParser, cartRout);
    app.use('/category', urlencodedParser, categoryRout);
    app.use('/home', urlencodedParser, homeRout);
    app.use('/order', urlencodedParser, orderRout);
    app.use('/product', urlencodedParser, productRout);


const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Trying to listen port ${PORT}.`);
})